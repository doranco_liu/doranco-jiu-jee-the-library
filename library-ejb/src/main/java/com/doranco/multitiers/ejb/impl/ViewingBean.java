package com.doranco.multitiers.ejb.impl;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.NoteDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.dao.ViewingDAO;
import com.doranco.multitiers.ejb.interfaces.INote;
import com.doranco.multitiers.ejb.interfaces.IViewing;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.doranco.multitiers.exceptions.ViewingNotFoundException;

@Stateless
public class ViewingBean implements IViewing {

	Logger log = Logger.getLogger(ViewingBean.class);

	@EJB
	ViewingDAO viewingDAO;

	@EJB
	UserDAO userDAO;

	@EJB
	BookDAO bookDAO;

	@Override
	public Viewing create(Viewing viewing) throws LibraryException {
		try {
			//Récuperation du book correspondant a viewing
			Book book = bookDAO.find(viewing.getBook().getId());
			viewing.setBook(book);
			
			//Récuperation de l'user correspondant a viewing
			User user = userDAO.findUser(viewing.getUser().getId());
			viewing.setUser(user);
			
			viewing = viewingDAO.create(viewing);
		} catch (Exception e) {
			log.error("Problème survenu lors de la création d'une viewing", e);
			throw new LibraryException(e);
		}
		
		return viewing;
	}

	@Override
	public Viewing update(Viewing viewing) throws LibraryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Viewing viewing) throws LibraryException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Viewing find(long userId, long bookId) throws LibraryException {
		log.debug("userId=" + userId + ", bookId=" + bookId);
		Viewing viewing = viewingDAO.findByUserIdAndBookId(userId, bookId);
		
		if(viewing.getDuration()*3600*1000 < new Date().getTime() - viewing.getStartDate().getTime()) {
			ViewingNotFoundException vnfe = new ViewingNotFoundException();
			
			log.error("Date d'expiration depassee", vnfe);
			
			throw vnfe;
		}
		
		return viewing;
	}

	
}
