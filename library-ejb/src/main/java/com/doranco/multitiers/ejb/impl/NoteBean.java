package com.doranco.multitiers.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.NoteDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.ejb.interfaces.INote;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;

@Stateless
public class NoteBean implements INote{
	
	Logger log = Logger.getLogger(NoteBean.class);
	
	@EJB
	NoteDAO noteDAO;
	
	@EJB
	UserDAO userDAO;
	
	@EJB
	BookDAO bookDAO;

	@Override
	public Note create(Note note) throws LibraryException {
		
		try {
			//Récuperation du book correspondant a noter
			Book book = bookDAO.find(note.getBook().getId());
			note.setBook(book);
			
			//Récuperation de l'user correspondant a noter
			User user = userDAO.findUser(note.getUser().getId());
			note.setUser(user);
			
			note = noteDAO.create(note);
		} catch (Exception e) {
			log.error("Problème survenu lors de la création d'une note", e);
			throw new LibraryException(e);
		}
		
		return note;
	}

	@Override
	public Note update(Note note) throws LibraryException {
		try {
			note = noteDAO.update(note);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return note;
	}

	@Override
	public void delete(long userId, long bookId) throws LibraryException {
		Note note = null;
		log.debug("userId=" + userId + ", bookId=" + bookId);
		note = noteDAO.findByUserIdAndBookId(userId, bookId);
		log.debug("Le note trouvé est : " + note);
		try {
			noteDAO.delete(note);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Note findByUserIdAndBookId(long userId, long bookId) throws LibraryException {
		log.debug("userId=" + userId + ", bookId=" + bookId);
		return noteDAO.findByUserIdAndBookId(userId, bookId);
	}

	
}
