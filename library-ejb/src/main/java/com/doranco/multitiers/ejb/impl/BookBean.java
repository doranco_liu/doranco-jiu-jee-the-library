package com.doranco.multitiers.ejb.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.ejb.interfaces.IBook;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.EntityNotFoundException;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Stateless
public class BookBean implements IBook {

	Logger log = Logger.getLogger(BookBean.class);
	
	@EJB
	BookDAO bookDAO;
	
	@Override
	public Book createBook(Book book) throws LibraryException {
		try {
			book = bookDAO.create(book);
		} catch (Exception e) {
			log.error("Problème lors de l'enregistrement d'un book", e);
			throw new LibraryException("Problème lors de l'enregistrement d'un book");
		}
		
		return book;
	}

	@Override
	public Book findBook(long id) throws LibraryException {
		Book book = null;
		
		book = bookDAO.find(id);
		
		if (book == null) throw new EntityNotFoundException();
		
		return book;
	}
	
	@Override
	public Page<Book> findBooks(String givenString, int pageSize, int pageNumber) throws LibraryException{
		Page<Book> books = null;
		books = bookDAO.findByIsbnOrTitle(givenString, pageSize, pageNumber);
		
//		if (books.isEmpty()) throw new EntityNotFoundException();
		
		return books;
		
	}

	@Override
	public List<Book> findAll() throws LibraryException {
		List<Book> books = null;
		books = bookDAO.findAll();
		
		if (books.isEmpty()) throw new EntityNotFoundException();
		
		return books;
	}

	@Override
	public Book updateBook(Book book) throws LibraryException {
		
		try {
			book = bookDAO.update(book);
		} catch (Exception e) {
			log.error("Problème lors de la modification d'un book", e);
			throw new LibraryException("Problème lors de la modification d'un book");
		}
		
		return book;
	}

	@Override
	public Book deleteBook(Book book) throws LibraryException {
		try {
			book = bookDAO.find(book.getId());
			bookDAO.delete(book);
		} catch (Exception e) {
			log.error("Problème lors de la suppresion d'un book", e);
			throw new LibraryException("Problème lors de la suppresion d'un book");
		}
		
		return book;
	}

}
