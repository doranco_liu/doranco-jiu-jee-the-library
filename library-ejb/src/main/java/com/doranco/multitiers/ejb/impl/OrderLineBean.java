package com.doranco.multitiers.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.OrderDAO;
import com.doranco.multitiers.dao.OrderLineDAO;
import com.doranco.multitiers.ejb.interfaces.IOrderLine;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.exceptions.LibraryException;

@Stateless
public class OrderLineBean implements IOrderLine{

	Logger log = Logger.getLogger(OrderLineBean.class);

	@EJB
	OrderDAO orderDAO;
	
	@EJB
	OrderLineDAO orderLineDAO;
	
	@EJB
	BookDAO bookDAO;

	@Override
	public OrderLine create(OrderLine orderLine) throws LibraryException {
		
		
		try {
			orderLine = orderLineDAO.create(orderLine);
		} catch (Exception e) {
			log.error("Problème lors de l'enregistrement d'un orderLine", e);
			throw new LibraryException(e);
		}
		
		return orderLine;
	}
	
	
}
