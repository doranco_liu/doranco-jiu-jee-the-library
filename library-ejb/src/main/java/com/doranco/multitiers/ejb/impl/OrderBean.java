package com.doranco.multitiers.ejb.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.OrderDAO;
import com.doranco.multitiers.dao.OrderLineDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.ejb.interfaces.IOrder;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;

@Stateless
public class OrderBean implements IOrder{

	Logger log = Logger.getLogger(OrderBean.class);

	@EJB
	OrderDAO orderDAO;
	
	@EJB
	UserDAO userDAO;
	
	@EJB
	OrderLineDAO orderLineDAO;
	
	@EJB
	BookDAO bookDAO;

	@Override
	public Order create(Order order) throws LibraryException {
		User user = null;
		Order newOrder;
		List<OrderLine> newOrderLines = new ArrayList<OrderLine>();
		try {
			//Recuperation de toutes les données du user correspond à l'id renvoyé depuis le client
			user = userDAO.findUser(order.getUser().getId());
			log.debug("le user trouvé est: " + user);
			//remplissage du user
			order.setUser(user);
			//Enregistrement de la commande
			newOrder = orderDAO.create(order);
			log.debug("le order apres la creation est: " + newOrder);
			
			//Enregistrer les lignes de commande à partir de la commande enregistrée
			for (OrderLine line : order.getLines()) {
				log.debug("le line est:" + line);
				Book book = bookDAO.find(line.getBook().getId());
				log.debug("le book trouvé est: " + book);
				line.setBook(book);
				line.setOrder(newOrder);
				line = orderLineDAO.create(line);
				log.debug("le line apres la creation est: " + line);
				newOrderLines.add(line);
			}
			newOrder.setLines(newOrderLines);
//			newOrder.setLines(order.getLines());
		} catch (Exception e) {
			log.error("Problème lors de l'enregistrement d'un order", e);
			throw new LibraryException(e);
		}
		
		return newOrder;
	}
	
}
