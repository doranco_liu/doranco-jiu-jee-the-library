package com.doranco.multitiers.ejb.impl;

import java.io.IOException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.EntityNotFoundException;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.doranco.multitiers.utils.KeyGenerator;
import com.doranco.multitiers.utils.Page;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Stateless
// Il faut commenter cette ligne, sinon on ne peut pas appeller ce bean à partir
// du projet Library-web
// @LocalBean
public class UserBean implements IUser {

	Logger log = Logger.getLogger(UserBean.class);

	@EJB
	UserDAO userDAO;

	@Override
	public User suscribe(User user) throws LibraryException {

		try {
			user = userDAO.create(user);
		} catch (Exception e) {
			log.error("Problème lors de l'inscription d'un utilisateur ", e);
			throw new LibraryException("Problème lors de l'inscription d'un utilisateur");
		}

		return user;
	}

	@Override
	public List<User> getAll() throws LibraryException {
		List<User> users = null;
		users = userDAO.getAll();
		
		if (users.isEmpty()) throw new EntityNotFoundException();
		
		return users;
		
//		try {
//			users = userDAO.getAll();
//		} catch (Exception e) {
//			log.error("Problème lors de la récupération des users", e);
//			throw new LibraryException();
//		}
//
//		return users;
	}

	@Override
	public Page<User> find(int pageSize, int pageNumber) throws LibraryException {
		Page<User> users = null;
		try {
			log.debug("pageSize=" + pageSize + ", pageNumber=" + pageNumber);
			users = userDAO.find(pageSize, pageNumber);
		} catch (Exception e) {
			log.error("Problème lors de la récupération des users", e);
			throw new LibraryException();
		}

		return users;
	}

	@Override
	public User getUser(long id) throws LibraryException {
		User user = null;
		user = userDAO.findUser(id);

		if (user == null || user.getUserName().isEmpty())
			throw new EntityNotFoundException();

		return user;
	}

	@Override
	public User createUser(User user) throws LibraryException {

		try {
			user = userDAO.create(user);
		} catch (Exception e) {
			log.error("Problème lors de l'enregistrement d'un utilisateur ", e);
			throw new LibraryException("Problème lors de l'enregistrement d'un utilisateur");
		}

		return user;
	}

	@Override
	public String connect(String userName, String password, String uri) throws LibraryException {
		String token = null;

		try {
			User user = userDAO.findByUserNameAndPassword(userName, password);

			token = issueToken(userName, uri, user.isAAdmin(), user.isSuperAdmin());
			
		} catch (EJBTransactionRolledbackException nre) {
			UserNotFoundException une = new UserNotFoundException(nre);
			log.error("Impossible de trouver un tel utilisateur ", nre);
			throw une;
			
//			EntityNotFoundException ene = new EntityNotFoundException(nre);
//			log.error("Impossible de trouver un tel utilisateur ", nre);
//			throw ene;
		} catch (Exception e) {
			log.error("Problème lors de la connexion d'un utilisateur ", e);
			throw new LibraryException("Problème lors de la connexion d'un utilisateur ");
		}

		return token;
	}

	private String issueToken(String login, String uriInfo, boolean setAdminClaim, boolean setSuperAdminClaim) throws NoSuchAlgorithmException, UnrecoverableKeyException,
			KeyStoreException, CertificateException, IOException {
		log.debug("URIINFO==> " + uriInfo);

		Key key = KeyGenerator.getInstance().getKey();
		log.debug("key dans la methode issueToken():" + key);
		
		JwtBuilder jwtBuilder = Jwts.builder().setSubject(login).setIssuer(uriInfo)
				.setIssuedAt(new Date()).setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)));
		
		List<String> roles = new ArrayList<String>();
		
		
		
		if(setAdminClaim) {
			roles.add(LibraryConstants.ADMIN_ROLE);
//			jwtBuilder.claim(LibraryConstants.JWT_ROLE_KEY, LibraryConstants.ADMIN_ROLE);
		}
		
		if(setSuperAdminClaim) {
			roles.add(LibraryConstants.SUPER_ADMIN_ROLE);
//			jwtBuilder.claim(LibraryConstants.JWT_ROLE_KEY, LibraryConstants.SUPER_ADMIN_ROLE);
		}
		
		jwtBuilder.claim(LibraryConstants.JWT_ROLE_KEY, roles);
		
		String jwtToken = jwtBuilder.signWith(SignatureAlgorithm.HS512, key).compact();

		log.info("Generation du token : " + jwtToken + " - " + key);
		return jwtToken;
	}

	private Date toDate(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	@Override
	public User setAsAdmin(long id) throws LibraryException {
		User user = null;
		try {
			user = userDAO.findUser(id);
			
			user.setAAdmin(true);
			userDAO.update(user);
		} catch (Exception e) {
			log.error("Problème lors de la definition d'un utilisateur comme administrateur", e);
			throw new LibraryException(e);
		}
		
		return user;
	}

}
