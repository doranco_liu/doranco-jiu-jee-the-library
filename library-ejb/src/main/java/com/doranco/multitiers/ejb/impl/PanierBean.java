package com.doranco.multitiers.ejb.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.ejb.Stateless;

import com.doranco.multitiers.ejb.interfaces.Panier;
import com.doranco.multitiers.exceptions.LibraryException;

@Stateless
public class PanierBean implements Panier {

	List<String> books = new ArrayList<String>(Arrays.asList("Twighlight", "Harray Potter", "Vampire Diaries",
			"Twighlight2", "Harray Potter2", "Vampire Diaries2"));

	@Override
	public void addBook(String book) throws LibraryException {
		books.add(book);
		removeDuplicateBook();
	}

	@Override
	public void removeBook(String book) throws LibraryException {
		books.remove(book);
		
		Iterator<String> i = books.iterator();
		while (i.hasNext())
		{
		    String s = i.next();
		    if (s == null || s.isEmpty())
		    {
		        i.remove();
		    }
		}
	}

	@Override
	public List<String> getBooks() throws LibraryException {
		List<String> results = null;
		try {
			// va aller recuperer les livres de la BD
			results = books;
		} catch (Exception e) {
			// loguer la stack strace dans le fichier de logs
			e.printStackTrace();
			throw new LibraryException("Problème survenu lors de la récuperation des livres");
		}

		return results;
	}

	@Override
	public int getPrix(String book) throws LibraryException {
		// TODO Auto-generated method stub
		if (book.equals("Twighlight")) {
			return 10;
		}
		return 1;
	}

	@Override
	public List<String> getProperties() throws LibraryException {
		Properties props = new Properties();
		String propsFileName = "jndi.properties";
		List<String> properties = new ArrayList<String>();

		try (InputStream resourceStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(propsFileName)) {
			props.load(resourceStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// get the property value and print it out
		properties.add(props.getProperty("localhost"));
		properties.add(props.getProperty("3900"));

		return properties;
	}

	@Override
	public void setBooksFromFile(String fileName) throws LibraryException {
		List<String> myList = new ArrayList<>();
		
		// Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				myList.add(line);
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.books.clear();
		this.books.addAll(myList);
	}

	@Override
	public String getFile(String fileName) throws LibraryException {

		StringBuilder result = new StringBuilder("");

		// Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result.toString();
	}

	@Override
	public void removeDuplicateBook() throws LibraryException {
		
		books = books.stream().distinct().collect(Collectors.toList());
	}

}
