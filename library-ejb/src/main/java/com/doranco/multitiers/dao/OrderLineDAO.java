package com.doranco.multitiers.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.entity.OrderLine;

@Stateless
@LocalBean
public class OrderLineDAO extends GeneralDAO<OrderLine> {

	private static Logger log = Logger.getLogger(OrderLineDAO.class);
	
}
