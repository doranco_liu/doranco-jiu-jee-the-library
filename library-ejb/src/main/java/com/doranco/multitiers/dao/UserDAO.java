package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.utils.Page;

@Stateless
@LocalBean
public class UserDAO extends GeneralDAO<User> {

	Logger log = Logger.getLogger(UserDAO.class);

	List<User> users = null;
	
	public User findByUserNameAndPassword(String givenUserName, String givenPassword) {
		String jpql = "SELECT u FROM User u WHERE u.userName = :userName AND u.password = :password";

		return (User) em.createQuery(jpql).setParameter("userName", givenUserName)
				.setParameter("password", givenPassword).getSingleResult();
	}

	public User findUser(long id) {
		User user = null;

		user = em.find(User.class, id);

		return user;
	}

	public List<User> getAll() {

		String jpql = "SELECT u FROM User u";

		users = (List<User>) em.createQuery(jpql).getResultList();

		return users;
	}

	public Page<User> find(int pageSize, int pageNumber) {
		String jpql = "SELECT u FROM User u";

		Query query = em.createQuery(jpql);
		query.setMaxResults(pageSize);
		query.setFirstResult(pageSize * pageNumber + 1);
		// récuperation d'une page d'utilisateurs
		@SuppressWarnings("unchecked")
		List<User> pageContent = query.getResultList();

		// récuperation du nombre total de users
		long totalCount = (long) (em.createQuery("SELECT count(*) FROM User u").getSingleResult());

		Page<User> page = new Page<User>(pageSize, pageNumber, totalCount, pageContent);

		return page;
	}

	public Page<Note> getNotes(long userId, int pageSize, int pageNumber) {
		String jpql = "SELECT n FROM Note n WHERE n.user.id= :userId";
		Query query = em.createQuery(jpql).setParameter("userId", userId).setMaxResults(pageSize)
				.setFirstResult(pageNumber * pageSize);

		// récuperation d'un page de notes
		List<Note> pageContent = query.getResultList();

		jpql = "SELECT count(*) FROM Note n WHERE n.user.id= :userId";
		long totalCount = (long) em.createQuery(jpql).setParameter("userId", userId).getSingleResult();

		Page<Note> page = new Page<Note>(pageSize, pageNumber, totalCount, pageContent);

		return page;
	}
	
	public Page<Order> getOrders(long userId, int pageSize, int pageNumber) {
		String jpql = "SELECT o FROM Order o WHERE o.user.id= :userId";
		Query query = em.createQuery(jpql).setParameter("userId", userId).setMaxResults(pageSize)
				.setFirstResult(pageNumber * pageSize);

		// récuperation d'un page de orders
		List<Order> pageContent = query.getResultList();

		jpql = "SELECT count(*) FROM Order o WHERE o.user.id= :userId";
		long totalCount = (long) em.createQuery(jpql).setParameter("userId", userId).getSingleResult();

		Page<Order> page = new Page<Order>(pageSize, pageNumber, totalCount, pageContent);

		return page;
	}
	
	public Page<Viewing> getViewings(long userId, int pageSize, int pageNumber) {
		String jpql = "SELECT v FROM Viewing v WHERE v.user.id= :userId";
		Query query = em.createQuery(jpql).setParameter("userId", userId).setMaxResults(pageSize)
				.setFirstResult(pageNumber * pageSize);

		// récuperation d'un page de viewings
		List<Viewing> pageContent = query.getResultList();

		jpql = "SELECT count(*) FROM Viewing v WHERE v.user.id= :userId";
		long totalCount = (long) em.createQuery(jpql).setParameter("userId", userId).getSingleResult();

		Page<Viewing> page = new Page<Viewing>(pageSize, pageNumber, totalCount, pageContent);

		return page;
	}

}
