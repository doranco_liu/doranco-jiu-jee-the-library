package com.doranco.multitiers.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import com.doranco.multitiers.entity.Note;

@Stateless
@LocalBean
public class NoteDAO extends GeneralDAO<Note> {

	private static Logger log = Logger.getLogger(NoteDAO.class);

	public Note find(long id) {
		Note note = null;

		note = em.find(Note.class, id);

		return note;
	}
	
	public Note findByUserIdAndBookId(long userId, long bookId){
		String jpql = "SELECT n FROM Note n WHERE n.user.id= :userId AND n.book.id= :bookId";
		log.debug("userId=" + userId + ", bookId=" + bookId);
		Query q = em.createQuery(jpql);
		q.setParameter("userId", userId).setParameter("bookId", bookId);
		
		Note note = (Note) q.getSingleResult();
		log.debug("Le note trouvé est :" + note);
		return note;
	}
	
}
