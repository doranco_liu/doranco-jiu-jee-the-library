package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;

import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.utils.Page;

@Stateless
@LocalBean
public class OrderDAO extends GeneralDAO<Order> {

	public Order findOrder(int id) {
		Order order = null;
		order = em.find(Order.class, (long) id);
		return order;
	}
	
	public Page<OrderLine> getOrderLines(long orderId, int pageSize, int pageNumber) {
		String jpql = "SELECT ol FROM OrderLine ol WHERE ol.order.id= :orderId";
		Query query = em.createQuery(jpql).setParameter("orderId", orderId).setMaxResults(pageSize)
				.setFirstResult(pageNumber * pageSize);

		// récuperation d'un page de viewings
		List<OrderLine> pageContent = query.getResultList();

		jpql = "SELECT count(*) FROM OrderLine ol WHERE ol.order.id= :orderId";
		long totalCount = (long) em.createQuery(jpql).setParameter("orderId", orderId).getSingleResult();

		Page<OrderLine> page = new Page<OrderLine>(pageSize, pageNumber, totalCount, pageContent);

		return page;
	}

}
