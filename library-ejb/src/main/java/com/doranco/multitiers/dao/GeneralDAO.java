package com.doranco.multitiers.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.utils.Page;

public class GeneralDAO<T> {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("library");
	EntityManager em = emf.createEntityManager();
	public EntityTransaction transaction = em.getTransaction();
	
	String jpql = null;

	protected GeneralDAO() {

	}

	private static GeneralDAO instance;

	public static GeneralDAO getInstance() {
		if (instance != null)
			return instance;

		synchronized (GeneralDAO.class) {
			instance = new GeneralDAO<>();
		}
		return instance;

	}

	public T create(T toBeCreated) throws Exception{
		transaction.begin();
//		em.persist(toBeCreated);
//		em.flush();
		
		toBeCreated=em.merge(toBeCreated);
		transaction.commit();
		return toBeCreated;
	}

	public void delete(T toBeDeleted) throws Exception{
		transaction.begin();
		em.remove(toBeDeleted);
		transaction.commit();
	}

	public T find(Class clazz, Long primaryKey) throws Exception{
		return (T) em.find(clazz, primaryKey);
	}

	public List<T> findAll(Class clazz) throws Exception{
		return em.createQuery("Select t from " + clazz.getSimpleName() + " t").getResultList();
	}

	public T update(T toBeUpdated) throws Exception{

		transaction.begin();
		toBeUpdated=em.merge(toBeUpdated);
		transaction.commit();
		return toBeUpdated;
	}
	
//	protected <T> Page<T> buildPage(int pageNumber, int pageSize, long totalCount, List<T> pageContent) {
//		Page<T> page = new Page<T>();
//		
//		page.setPageSize(pageSize);
//		page.setPageNumber(pageNumber);
//		page.setTotalCount(totalCount);
//		page.setContent(pageContent);
//
//		return page;
//	}

}
