package com.doranco.multitiers.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.LibraryException;

@Stateless
@LocalBean
public class ViewingDAO extends GeneralDAO<Viewing> {

	private static Logger log = Logger.getLogger(ViewingDAO.class);
	
	public Viewing findByUserIdAndBookId(long userId, long bookId) throws LibraryException{
		Viewing viewing = null;
		
		String jpql = "SELECT v FROM Viewing v WHERE v.user.id= :userId AND v.book.id= :bookId";
		log.debug("userId=" + userId + ", bookId=" + bookId);
		Query q = em.createQuery(jpql);
		q.setParameter("userId", userId).setParameter("bookId", bookId);
		
		viewing = (Viewing) q.getSingleResult();
		log.debug("Le viewing trouvé est :" + viewing);
		return viewing;
	}
}
