package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.utils.Page;

@Stateless
@LocalBean
public class BookDAO extends GeneralDAO<Book> {

	private static Logger log = Logger.getLogger(BookDAO.class);
	
	public Book find(long id) {
		Book book = null;
		
		book = em.find(Book.class, id);
		
		return book;
	}
	
//	public List<Book> findByIsbnOrTitle(String givenString) {
//		String jpql = "SELECT b FROM Book b WHERE b.title like :match OR b.isbn like :match";
//		
//		Query q = em.createQuery(jpql);
//		q.setParameter("match", "%" + givenString + "%");
//		log.debug("le parametre actuelle est: " + q.getParameterValue("match"));
//		
//		List<Book> books = q.getResultList();
//		
//		return books;
//	}
	
	public Page<Book> findByIsbnOrTitle(String queryString, int pageSize, int pageNumber) {
		String jpql = "SELECT b FROM Book b WHERE b.title like CONCAT('%', :query, '%')" 
				+ " OR b.isbn like CONCAT('%', :query, '%')"; 
		
		Query q = em.createQuery(jpql);
		q.setParameter("query", queryString).setMaxResults(pageSize).setFirstResult(pageNumber*pageSize);
		log.debug("le parametre actuelle est: " + q.getParameterValue("query"));
		
		List<Book> pageContent = q.getResultList();
		
		jpql = "SELECT count(*) FROM Book b WHERE b.title like CONCAT('%', :query, '%')" 
				+ " OR b.isbn like CONCAT('%', :query, '%')";
		long totalCount = em.createQuery(jpql).setParameter("query", queryString).getFirstResult();
		
		Page<Book> page = new Page<Book>(pageSize, pageNumber, totalCount, pageContent);
				
		return page;
	}
	
	public List<Book> findAll(){

		jpql = "SELECT b FROM Book b";

		List<Book> books = (List<Book>) em.createQuery(jpql).getResultList();

		return books;
	}
	
	public Page<Note> getNotes(long bookId, int pageSize, int pageNumber) {
		String jpql = "SELECT n FROM Note n WHERE n.book.id= :bookId";
		Query query = em.createQuery(jpql).setParameter("bookId", bookId).setMaxResults(pageSize)
				.setFirstResult(pageNumber * pageSize);

		// récuperation d'un page de viewings
		List<Note> pageContent = query.getResultList();

		jpql = "SELECT count(*) FROM Note n WHERE n.book.id= :bookId";
		long totalCount = (long) em.createQuery(jpql).setParameter("bookId", bookId).getSingleResult();

		Page<Note> page = new Page<Note>(pageSize, pageNumber, totalCount, pageContent);

		return page;
	}
	
	public Page<Viewing> getViewings(long bookId, int pageSize, int pageNumber) {
		String jpql = "SELECT v FROM Viewing v WHERE v.book.id= :bookId";
		Query query = em.createQuery(jpql).setParameter("bookId", bookId).setMaxResults(pageSize)
				.setFirstResult(pageNumber * pageSize);

		// récuperation d'un page de viewings
		List<Viewing> pageContent = query.getResultList();

		jpql = "SELECT count(*) FROM Viewing v WHERE v.book.id= :bookId";
		long totalCount = (long) em.createQuery(jpql).setParameter("bookId", bookId).getSingleResult();

		Page<Viewing> page = new Page<Viewing>(pageSize, pageNumber, totalCount, pageContent);

		return page;
	}
	
}
