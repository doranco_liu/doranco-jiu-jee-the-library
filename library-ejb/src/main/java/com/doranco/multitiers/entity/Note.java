package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="l_note")
@IdClass(IdNote.class)
public class Note implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2641901594165347498L;

	// id attribute mapped by join column default
	@Id
	@ManyToOne
	private User user;
	
	@Id
	@ManyToOne
	private Book book;
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}


	private Date noteDate;
	private String comment;
	private int value;
	
	
	public Note() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Date getNoteDate() {
		return noteDate;
	}


	public void setNoteDate(Date noteDate) {
		this.noteDate = noteDate;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public int getValue() {
		return value;
	}


	public void setValue(int value) {
		this.value = value;
	}

	
	
}
