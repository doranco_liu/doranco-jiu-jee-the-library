package com.doranco.multitiers.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class IdViewing implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 400555698775674854L;

	private long user;

	private long book;

	public IdViewing() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getUser() {
		return user;
	}

	public void setUser(long user) {
		this.user = user;
	}

	public long getBook() {
		return book;
	}

	public void setBook(long book) {
		this.book = book;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdViewing other = (IdViewing) obj;
		if (user != other.user)
			return false;
		if (book != other.book)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) user;
		result = prime * result + (int) book;
		return result;
	}
}
