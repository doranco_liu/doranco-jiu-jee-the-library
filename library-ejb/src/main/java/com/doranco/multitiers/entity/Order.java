package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="l_order")
public class Order extends Identifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5381637714980814252L;

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	@ManyToOne
	private User user;
	
	@OneToMany(mappedBy="order", fetch=FetchType.LAZY)
	private Set<OrderLine> orderLines;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(Set<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}
	
	
}
