
DROP DATABASE IF EXISTS ze_library;
create DATABASE ze_library;
grant all on ze_library.* to 'library' identified by 'library';

use ze_library;
