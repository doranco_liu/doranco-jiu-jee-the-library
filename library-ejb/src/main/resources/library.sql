
DROP DATABASE IF EXISTS library;
create DATABASE library;
grant all on library.* to 'library' identified by 'library';

use library;

DROP TABLE IF EXISTS `L_CART`;

CREATE TABLE `L_CART` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `L_BOOK`;

CREATE TABLE `L_BOOK` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ISBN` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,  
  PRIMARY KEY (`id`)
  
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `L_BOOK_COPY`;

CREATE TABLE `L_BOOK_COPY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `book_id` bigint(20) NOT NULL,
  `cart_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`book_id`) REFERENCES L_BOOK(`id`),
  FOREIGN KEY (`cart_id`) REFERENCES L_CART (`id`)
  
) ENGINE=InnoDB DEFAULT CHARSET=latin1;