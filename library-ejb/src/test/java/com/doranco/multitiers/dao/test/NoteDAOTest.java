package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.NoteDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;

@RunWith(Arquillian.class)
public class NoteDAOTest {

	private static Logger log = Logger.getLogger(NoteDAOTest.class);

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class, "test.jar").addClass(NoteDAO.class).addClass(UserDAO.class).addClass(BookDAO.class)
		// .addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml")
		;
	}

	@EJB
	NoteDAO dao;
	
	@EJB
	UserDAO userDAO;
	
	@EJB
	BookDAO bookDAO;

	List<Note> notes;
	
//	@Test
	@InSequence(1)
	public void shouldCreateNoteSansUB() {
		try {
			Note note;
			User user;
			Book book;

			for (int i = 1; i < 5; i++) {
				note = new Note();
				note.setComment("commentNoteSansUB" + i);
				note.setValue(i);
				note.setNoteDate(new Date());
				
				user = new User();
				user.setFirstName("firstNameNoteSansUB" + i);
				user.setLastName("lastNameNoteSansUB" + i);
				user.setUserName("userNameNoteSansUB" + i);
				user.setPassword("passwordNoteSansUB" + i);
				note.setUser(user);
				
				book = new Book();
				book.setIsbn("123412341"+i);
				book.setTitle("titleNoteSansUB"+i);
				note.setBook(book);
				
				note = dao.create(note);
				log.debug("Le note cré est " +note);
			}
		} catch (Exception e) {
			fail("Ne devrait pas retourner d'exception");
		}
		
	}
	
//	@Test
	@InSequence(2)
	public void shouldCreateNoteAvecUB() {
		try {
			Note note;
			User user;
			Book book;
			
			for (int i = 1; i < 5; i++) {
				note = new Note();
				note.setComment("commentAvecUB" + i);
				note.setValue(i);
				note.setNoteDate(new Date());
				
				book=bookDAO.find(i);
				log.debug("Le book trouvé est " +book);
				user=userDAO.findUser(i);
				log.debug("Le user trouvé est " +user);
				
				
				note.setUser(user);
				note.setBook(book);
				
				note = dao.create(note);
//				note = dao.update(note);
				
				log.debug("Le note cré est " +note);
			}
			
//			notes = dao.findAll(Note.class);
//			log.debug("Les notes dans la BD sont: " + notes);
		} catch (Exception e) {
			fail("Ne devrait pas retourner d'exception");
		}
		
	}
	
//	@Test
	@InSequence(3)
	public void testCreate() {
		try {
			Note note;
			User user;
			Book book;
			
			for (int i = 1; i < 5; i++) {
				note = new Note();
				note.setComment("commentAvecUB" + i);
				note.setValue(i);
				note.setNoteDate(new Date());
				
				user = new User();
				user.setFirstName("firstNameNoteSansUB" + i);
				user.setLastName("lastNameNoteSansUB" + i);
				user.setUserName("userNameNoteSansUB" + i);
				user.setPassword("passwordNoteSansUB" + i);
				user = userDAO.create(user);
				log.debug("Le user cré est " +user);
				user = userDAO.findUser(user.getId());
				
				book = new Book();
				book.setIsbn("123412341" + i);
				book.setTitle("titleNoteSansUB" + i);
				book = bookDAO.create(book);
				log.debug("Le book cré est " +book);
				book = bookDAO.find(book.getId());
				
				note.setUser(user);
				note.setBook(book);
				
				note = dao.create(note);
				log.debug("Le note cré est " +note);
			}
			
//			note = new Note();
//			note.setComment("commentAvecUB");
//			note.setValue(1);
//			note.setNoteDate(new Date());
//			
//			user = new User();
//			user.setFirstName("firstNameNoteSansUB");
//			user.setLastName("lastNameNoteSansUB");
//			user.setUserName("userNameNoteSansUB");
//			user.setPassword("passwordNoteSansUB");
//			user = userDAO.create(user);
//			log.debug("Le user cré est " +user);
//			user = userDAO.findUser(user.getId());
//			
//			book = new Book();
//			book.setIsbn("1234123411");
//			book.setTitle("titleNoteSansUB");
//			book = bookDAO.create(book);
//			log.debug("Le book cré est " +book);
//			book = bookDAO.find(book.getId());
//			
//			note.setUser(user);
//			note.setBook(book);
//			
//			note = dao.create(note);
//			log.debug("Le note cré est " +note);
			
		} catch (Exception e) {
			fail("Ne devrait pas retourner d'exception");
		}
		
	}
	
	@Test
	@InSequence(4)
	public void testCreateNew() {
		try {
			Note note;
			User user;
			Book book;
			
			note = new Note();
			note.setComment("commentAvecUB");
			note.setValue(1);
			note.setNoteDate(new Date());
			
			user = userDAO.findUser(1);
			log.debug("Le user trouvé est " +user);
			
			book = bookDAO.find(1);
			log.debug("Le book trouvé est " +book);
			
			note.setUser(user);
			note.setBook(book);
			
			note = dao.create(note);
			log.debug("Le note cré est " +note);
			
		} catch (Exception e) {
			fail("Ne devrait pas retourner d'exception");
		}
		
	}
	
//	@Test
	@InSequence(5)
	public void testCreateAvecUB() {
		try {
			Note note;
			User user;
			Book book;
			
			note = new Note();
			note.setComment("commentAvecUB");
			note.setValue(1);
			note.setNoteDate(new Date());
			
			user = userDAO.findUser(1);
			log.debug("Le user trouvé est " +user);
			
			book = bookDAO.find(1);
			log.debug("Le book trouvé est " +book);
			
			note.setUser(user);
			note.setBook(book);
			
			note = dao.create(note);
			log.debug("Le note cré est " +note);
			
		} catch (Exception e) {
			fail("testCreateAvecUB ne devrait pas retourner d'exception");
		}
		
	}
	
	@Test
	@InSequence(6)
	public void testUpdateANote() {
		try {
			Note note = dao.findByUserIdAndBookId(1, 1);
			log.debug("Le note trouvé est " +note);
			note.setComment("commentAvecUBmodification");
			note = dao.update(note);
			
			assertEquals(note.getComment(), "commentAvecUBmodification");
			
		} catch (Exception e) {
			log.error("Error = ", e);
			fail("testUpdateANote ne devrait pas retourner d'exception");
		}
		
	}
	
//	@Test(expected=EJBException.class)
	@InSequence(7)
	public void testDeleteANote() {
		Note note = null;
		
		try {
			note = dao.findByUserIdAndBookId(1, 1);
			log.debug("Le note trouvé est " +note);
			dao.delete(note);
		} catch (Exception e) {
			log.error("Error = ", e);
			fail("testDeleteANote ne devrait pas retourner d'exception");
		}
		
		note = dao.findByUserIdAndBookId(1, 1);
	}
	
	@Test
	@InSequence(8)
	public void testFindByUserIdAndBookId() {
		Note note = null;
		
		note = dao.findByUserIdAndBookId(1, 1);
		
		log.debug("Le note trouvé est " +note);
		
		assertNotNull(note);
	}
	
	
}
