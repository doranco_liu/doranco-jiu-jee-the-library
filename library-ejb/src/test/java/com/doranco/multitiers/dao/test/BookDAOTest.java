package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.utils.Page;

@RunWith(Arquillian.class)
public class BookDAOTest {

	private static Logger log = Logger.getLogger(BookDAOTest.class);
	
	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class, "test.jar").addClass(BookDAO.class)
		// .addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml")
		;
	}

	@EJB
	BookDAO dao;
	
	List<Book> books;
	
//	@Test
	@InSequence(1)
	public void testCreate() {
		try {
			Book book;
			for (int i = 1; i < 6; i++) {
				book = new Book();
				book.setIsbn("123412341" + i);
				book.setTitle("title" + i);
				
				book = dao.create(book);
				log.debug("Le book cré est " +book);
			}
			
			books = dao.findAll(Book.class);
			log.debug("Les books dans la BD sont: " + books);
		} catch (Exception e) {
			fail("Ne devrait pas retourner d'exception");
		}
		
	}
	
//	@Test
	@InSequence(2)
	public void testFindWithId() {
		
		Book  book = null;
		
		book = dao.find(1);

		log.debug("le book trouvé est: " + book);

		assertNotNull(book);
		
	}
	
	@Test
	@InSequence(3)
	public void testFindByIsbnOrTitle(){
		
		Page<Book>  books = null;
		String queryString = "isbn";
		int pageSize = 3;
		int pageNumber = 0;
		
		books = dao.findByIsbnOrTitle(queryString, pageSize, pageNumber);

		log.debug("les books trouvés sont: " + books);

		assertNotNull(books);
		
		queryString = "title";
		books = dao.findByIsbnOrTitle(queryString, pageSize, pageNumber);

		log.debug("les books trouvés sont: " + books);

		assertNotNull(books);
		
	}
	
//	@Test
	@InSequence(4)
	public void testUpdate(){
		Book book = dao.find(15);
		log.debug("le book à modifier est: " + book);
		
		String title = book.getTitle() + "modification";
		book.setTitle(title);
		
		try {
			book = dao.update(book);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.debug("le book apres la modification est: " + dao.find(15));
		assertEquals(book.getTitle(), title);
	}
	
//	@Test
	@InSequence(5)
	public void testDelete(){
		Book book = dao.find(16);
		log.debug("le book à supprimer est: " + book);
		
		try {
			dao.delete(book);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		assertNull(dao.find(16));
	}
	
	
//	@Test
	@InSequence(2)
	public void shouldModifierABook() {
		try {
			books = dao.findAll(Book.class);
			
			for (Book book : books) {
				log.debug("le book à modifier est: " + book);
				
				assertNotNull(book);
				
				book.setIsbn(book.getIsbn() + "modification");
				book.setTitle(book.getTitle() + "modification");
				
				dao.update(book);
			}
			books = dao.findAll(Book.class);
			log.debug("Les books dans la BD sont: " + books);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
//	@Test
	@InSequence(3)
	public void shouldDeleteABook() {
		try {
			books = dao.findAll(Book.class);
			
			for (Book book : books) {
				log.debug("le book à supprimer est: " + book);
				
				assertNotNull(book);
				
				dao.delete(book);
			}
			books = dao.findAll(Book.class);
			log.debug("Les books dans la BD sont: " + books);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
