package com.doranco.multitiers.dao.test;

import static org.junit.Assert.fail;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;


public class DAOTest {

	private static Logger log = Logger.getLogger(DAOTest.class);
	
	@Test
	@InSequence(1)
	public void shouldGeneratDataBase() {
		try {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("library");
			EntityManager em = emf.createEntityManager();
			log.debug("EntityManager em est: " + em);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erreur lors de l'initialisation d'hibernate");
		}
	}
	
}
