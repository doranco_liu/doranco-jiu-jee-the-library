package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;

@RunWith(Arquillian.class)
public class UserDAOTest {

	private static Logger log = Logger.getLogger(UserDAOTest.class);

	// Pour la methode deployment d'un test arquillian
	// arquillian.org/guides/getting_started/#write_an_arquillian_test
	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class).addClasses(UserDAO.class);
	}

	@EJB
	private UserDAO dao;

	List<User> users;

	@Test
	@InSequence(1)
	public final void testCreate() {
//		User user = new User();
//		user.setFirstName("firstName1");
//		user.setLastName("lastName1");
//		user.setUserName("userName1");
//		user.setPassword("password1");
//		log.debug("user ici: " + user);
//
//		try {
//			dao.create(user);
//
//			log.debug("user cré est: " + user);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		try {
			users = new ArrayList<User>();
			User user = null;
			for (int i = 1; i < 6; i++) {
				user = new User();
				user.setFirstName("firstName" + i);
				user.setLastName("lastName" + i);
				user.setUserName("userName" + i);
				user.setPassword("password" + i);
				if ((i & 1) == 0) {
					user.setAAdmin(true);
				}
				log.debug("Utilisateur cré est: " + user);

				dao.create(user);
			}
			
//			users = dao.findAll(User.class);
			
			users = dao.getAll();
			
			log.debug("Les users dans la BD sont: " + users);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
//		users = new ArrayList<User>();
//		User user = null;
//		for (int i = 1; i < 6; i++) {
//			user = new User();
//			user.setFirstName("firstName" + i);
//			user.setLastName("lastName" + i);
//			user.setUserName("userName" + i);
//			user.setPassword("password" + i);
//			if ((i & 1) == 0) {
//				user.setAAdmin(true);
//			}
//			log.debug("Utilisateur cré est: " + user);
//
//			dao.create(user);
//		}
//
//		users = dao.findAll(User.class);
//		log.debug("Les users dans la BD sont: " + users);
	}

//	@Test
	@InSequence(2)
	public final void shouldNotFindAUserWithBadPassword() throws LibraryException {
		User user = null;

		try {
			user = dao.findByUserNameAndPassword("userName", "password");

		} catch (EJBException e) {

		}

		assertNull(user);

	}

//	@Test
	@InSequence(3)
	public final void shouldFindAUserWithGoodPassword() throws LibraryException {
		User user;

		user = dao.findByUserNameAndPassword("userName1", "password1");
		assertNotNull(user);
		log.debug("Utilisateur trouvé est: " + user);

//		try {
//			for (int i = 1; i < 6; i++) {
//				user = dao.findByUserNamePassword("userName" + i, "password" + i);
//				log.debug("Utilisateur trouvé est: " + user);
//
//				assertNotNull(user);
//			}
//		} catch (Exception e) {
//			log.error("Error = ", e);
//			fail("Ne devrait pas retourner d'exception lors de la récuperation d'un user avec des bons identifiers");
//		}

	}

//	@Test
	@InSequence(4)
	public final void testFind() throws LibraryException {
		User user = null;

		user = dao.findByUserNameAndPassword("userName1", "password1");
		log.debug("le user trouvé1 est: " + user);

		user = dao.findUser((long) user.getId());
		log.debug("le user trouvé2 est: " + user);

		assertNotNull(user);
	}

//	@Test
	@InSequence(5)
	public final void testUpdate() throws LibraryException {
		User user = null;
		String newUserName = "userName2";
		String newPassword = "password2";

		user = dao.findByUserNameAndPassword("userName1", "password1");

		user.setUserName(newUserName);
		user.setPassword(newPassword);

		try {
			user = dao.update(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertTrue(user.getUserName().equals(newUserName) && user.getPassword().equals(newPassword));
	}

//	@Test
	@InSequence(6)
	public final void testDelete() {
		try {
			users = dao.findAll(User.class);

			for (User user : users) {
				log.debug("le user à supprimer est: " + user);

				assertNotNull(user);

				dao.delete(user);
			}
			users = dao.findAll(User.class);
			log.debug("Les users dans la BD sont: " + users);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
