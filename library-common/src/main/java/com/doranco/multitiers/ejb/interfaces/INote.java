package com.doranco.multitiers.ejb.interfaces;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.exceptions.LibraryException;

@Remote
public interface INote {

	public Note create(Note note) throws LibraryException;
	
	public Note update(Note note) throws LibraryException;
	
//	public void delete(Note note) throws LibraryException;
	
	public void delete(long userId, long bookId) throws LibraryException;
	
	public Note findByUserIdAndBookId(long userId, long bookId) throws LibraryException;
	
}
