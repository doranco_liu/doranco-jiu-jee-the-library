package com.doranco.multitiers.ejb.interfaces;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.exceptions.LibraryException;

@Remote
public interface IOrderLine {

	public OrderLine create(OrderLine orderLine) throws LibraryException;
}
