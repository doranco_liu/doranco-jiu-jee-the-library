package com.doranco.multitiers.ejb.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.doranco.multitiers.exceptions.LibraryException;

@Remote
public interface Panier {

	public void addBook(String book) throws LibraryException;
	
	public void removeBook(String book) throws LibraryException;
	
	public void removeDuplicateBook() throws LibraryException;
	
	public List<String> getBooks() throws LibraryException;
	
	public int getPrix(String book) throws LibraryException;
	
	public List<String> getProperties() throws LibraryException;
	
	public void setBooksFromFile(String fileName) throws LibraryException;
	
	public String getFile(String fileName) throws LibraryException;
}
