package com.doranco.multitiers.ejb.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Remote
public interface IBook {

	public Book createBook(Book book) throws LibraryException;
	
	public Book findBook(long id) throws LibraryException;
	
	public Page<Book> findBooks(String givenString, int pageSize, int pageNumber) throws LibraryException;
	
	public List<Book> findAll() throws LibraryException;
	
	public Book updateBook(Book book) throws LibraryException;
	
	public Book deleteBook(Book book) throws LibraryException;
}
