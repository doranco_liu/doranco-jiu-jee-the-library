package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name="l_book")
public class Book extends Identifier implements Serializable{

	private static final long serialVersionUID = 4484249362256718453L;
	
	@NotNull
	@Pattern(regexp="^\\d{9}[\\d|X]$")
	private String isbn;
	@NotNull
	private String title;
	
	@Lob
	@NotNull
	private byte[] content;
	
	public Book() {
		
	}


	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}
	
	
}
