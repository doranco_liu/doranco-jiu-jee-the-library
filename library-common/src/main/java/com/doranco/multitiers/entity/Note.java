	package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="l_note")
@IdClass(IdNote.class)
public class Note implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2641901594165347498L;

	// id attribute mapped by join column default
	@Id
	@ManyToOne
//	@JoinColumn(name = "user_id")
	@NotNull
	private User user;
	
	@Id
	@ManyToOne
//	@JoinColumn(name = "book_id")
	@NotNull
	private Book book;
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}


//	@NotNull
	private Date noteDate=new Date() ;
	@Size(min=0, max=240)
	private String comment;
	@NotNull
	@Max(5)
	@Min(0)
	private int value;
	
	
	public Note() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Date getNoteDate() {
		return noteDate;
	}


	public void setNoteDate(Date noteDate) {
		this.noteDate = noteDate;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public int getValue() {
		return value;
	}


	public void setValue(int value) {
		this.value = value;
	}

//	@Override
//	public String toString() {
//		return "Note [user=" + user + ", book=" + book + ", noteDate=" + noteDate + ", comment=" + comment + ", value="
//				+ value + "]";
//	}

	@Override
	public String toString() {
		return "Note [userId=" + user.getId() + ", bookId=" + book.getId() + ", noteDate=" + noteDate + ", comment=" + comment + ", value="
				+ value + "]";
	}

	
	
}
