package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="l_user")
public class User extends Identifier implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2635096268677413348L;
	
	private String firstName;
	
	@NotNull
	private String lastName;
	
	@NotNull
	private String userName;
	
//	@Pattern(regexp="^.*(?=.{8,})(?=..*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$")
	@NotNull
	private String password;
	
	private boolean isAAdmin = false;
	
	private boolean isSuperAdmin = false;
	

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAAdmin() {
		return isAAdmin;
	}

	public void setAAdmin(boolean isAAdmin) {
		this.isAAdmin = isAAdmin;
	}

	public boolean isSuperAdmin() {
		return isSuperAdmin;
	}

	public void setSuperAdmin(boolean isSuperAdmin) {
		this.isSuperAdmin = isSuperAdmin;
	}

	
}
