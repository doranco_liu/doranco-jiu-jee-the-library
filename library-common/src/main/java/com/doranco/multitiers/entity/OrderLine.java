package com.doranco.multitiers.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="l_order_line")
@IdClass(IdOrderLine.class)
public class OrderLine implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5286985899509916690L;

	@Id
	@ManyToOne
	private Order order;
	
	@Id
	@ManyToOne
	private Book book;

	public OrderLine() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@Override
	public String toString() {
		if(order!=null && book!=null) {
			return "OrderLine [orderId=" + order.getId() + ", bookId=" + book.getId() + "]";
			
		} else if(book!=null) {
			return "OrderLine [bookId=" + book.getId() + "]";
		} else if(order!=null) {
			return "OrderLine [orderId=" + order.getId() + "]";
		} else return "OrderLine [orderId=null , bookId=null]";
	}

	
	
}
