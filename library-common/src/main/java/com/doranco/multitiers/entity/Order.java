package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="l_order")
public class Order extends Identifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5381637714980814252L;

	@ManyToOne
	private User user;
	
	@Transient
	private List<OrderLine> lines;
	
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public List<OrderLine> getLines() {
		return lines;
	}


	public void setLines(List<OrderLine> lines) {
		this.lines = lines;
	}


	@Override
	public String toString() {
		return "Order [user=" + user + ", lines=" + lines + ", id=" + getId() + "]";
	}


	
	
	
	

}
