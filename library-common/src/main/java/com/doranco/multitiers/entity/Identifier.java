package com.doranco.multitiers.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Identifier implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1758094114295570783L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	public Identifier() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Identifier [id=" + id + "]";
	}
	
	
}