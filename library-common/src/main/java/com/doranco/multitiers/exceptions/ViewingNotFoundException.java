package com.doranco.multitiers.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

@Provider
public class ViewingNotFoundException extends EntityNotFoundException {

	public ViewingNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ViewingNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ViewingNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ViewingNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ViewingNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	

}
