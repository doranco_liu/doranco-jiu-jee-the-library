package com.doranco.multitiers.mappers.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.mappers.OrderLineMapper;
import com.doranco.multitiers.model.vm.OrderLineVM;

public class OrderLineMapperTest {

	@Test
	public void shouldEntityToVM() {
		// given
		OrderLine orderLine = new OrderLine();
		Book book = new Book();
		
		orderLine.setBook(book);
		
		// when
		OrderLineVM orderLineVM = OrderLineMapper.INSTANCE.entityToVM(orderLine);

		// then
		assertNotNull(orderLineVM);
		assertEquals((long)book.getId(), orderLineVM.getIdBook());
	}
}
