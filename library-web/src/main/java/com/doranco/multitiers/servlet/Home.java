package com.doranco.multitiers.servlet;

import java.io.IOException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.Panier;
import com.doranco.multitiers.exceptions.LibraryException;

/**
 * Servlet implementation class Home
 */
@WebServlet("/")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//@EJB //(lookup = "corbaname:iiop:localhost:3900#java:global/library-ejb/PanierBean")
	Panier panier;
	
    /**
     * Default constructor. 
     */
    public Home() {
    	super();
    	
    	try {
			panier = (Panier)RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/PanierBean");
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	/*try {
    		InitialContext ic = new InitialContext();
    		panier = (Panier)ic.lookup("java:global/library-ejb/PanierBean");
    		
    		panier = (Panier)RemoteContext.getInstance().getInitialContext();
		} catch (Exception e) {
			// TODO: handle exception
		}*/
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			panier.addBook(request.getParameter("ajout_book_name"));
			//panier.addBook("Essai");
			
			panier.removeBook(request.getParameter("supp_book_name"));
			//panier.removeBook("Harray Potter2");
			
			//panier.setBooksFromFile();
			
			List<String> books = panier.getBooks();
			request.setAttribute("books", books);
//			int prix = panier.getPrix("Twighlight");
//			request.setAttribute("prix", prix);
			
			
//			List<String> jdniProperties = panier.getProperties();
//			System.out.println("jdniProperties est: " + jdniProperties);
//			request.setAttribute("jdniProperties", jdniProperties);
			
//			List<String> newBooks = panier.setBooksFromFile();
//			request.setAttribute("newBooks", newBooks);
			
//			request.setAttribute("stringBooks", panier.getFile("file/test.txt"));
//			
//			panier.setBooksFromFile("file/books.txt");
//			request.setAttribute("newBooks", panier.getBooks());
//			
//			panier.removeDuplicateBook();
//			request.setAttribute("newSansDuplicateBooks", panier.getBooks());
			
			this.getServletContext().getRequestDispatcher("/jsp/index.jsp").forward(request, response);
		} catch (LibraryException e) {
			System.out.println(e.getMessage());
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
