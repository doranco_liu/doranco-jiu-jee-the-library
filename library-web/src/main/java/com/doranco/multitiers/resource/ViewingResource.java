package com.doranco.multitiers.resource;

import java.net.URI;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IViewing;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.mappers.ViewingMapper;
import com.doranco.multitiers.model.vm.ViewingVM;

@Path("viewings")
@Produces(MediaType.APPLICATION_JSON)
public class ViewingResource {

	Logger log = Logger.getLogger(ViewingResource.class);

//	BookMapper bookMapper = BookMapper.INSTANCE;
	ViewingMapper viewingMapper = ViewingMapper.INSTANCE;
	
	IViewing iViewing = null;
	
	public ViewingResource() throws LibraryException {
		super();
		
		try {
			log.debug("Avant de BookResource lookup");
			iViewing = (IViewing) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/ViewingBean");
			log.debug("Fin de ViewingResource constructeur!");
		} catch (NamingException e) {
			log.error("Impossible de démarrer de la resource ViewingResource!", e);
			throw new LibraryException(e);
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(ViewingVM viewingVM) 
			throws LibraryException {
		Viewing viewing = null;
		log.debug("Try to create un viewing a partir d'un viewingVM: " + viewingVM);

		viewing = viewingMapper.vMToEntity(viewingVM);
		
		viewing = iViewing.create(viewing);
		
		viewingVM = viewingMapper.entityToVM(viewing);

//		return Response.ok().entity(viewingVM).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "viewing cree avec succès").build();
		
		URI uri = UriBuilder.fromResource(ViewingResource.class).build();
		 
		return Response.created(uri).entity(viewingVM).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "viewing cré avec succès").build();
	}
	
}
