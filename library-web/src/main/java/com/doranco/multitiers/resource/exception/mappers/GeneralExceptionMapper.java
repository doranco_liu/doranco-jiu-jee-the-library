package com.doranco.multitiers.resource.exception.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

@Provider
public class GeneralExceptionMapper implements ExceptionMapper<Exception> {

	Logger log = Logger.getLogger(GeneralExceptionMapper.class);

	@Override
	public Response toResponse(Exception exception) {
		log.error("Problème d'ordre général et inconnu", exception);

		return Response.status(Status.INTERNAL_SERVER_ERROR).header("Server Message",
				"Un problème inconnu est survenu lors du traitement de votre requête, veuillez réessayer plus tard, nos ingénieurs sont déjà sur le coup")
				.build();
	}

}