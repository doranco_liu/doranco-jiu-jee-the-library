package com.doranco.multitiers.resource.exception.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.doranco.multitiers.exceptions.LibraryException;

@Provider
public class LibraryExceptionMapper implements ExceptionMapper<LibraryException> {

	Logger log = Logger.getLogger(LibraryExceptionMapper.class);

	@Override
	public Response toResponse(LibraryException exception) {
		log.error("Problème d'ordre général", exception);

		return Response.status(Status.INTERNAL_SERVER_ERROR).header("Server Message",
				"Problème survenu lors du traitement de votre requête, "
				+ "veuillez réessayer plus tard").build();
	}

}