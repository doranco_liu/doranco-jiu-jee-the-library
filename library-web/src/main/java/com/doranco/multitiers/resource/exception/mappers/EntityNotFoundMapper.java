package com.doranco.multitiers.resource.exception.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;
import com.doranco.multitiers.exceptions.EntityNotFoundException;

@Provider
public class EntityNotFoundMapper implements ExceptionMapper<EntityNotFoundException>{

	Logger log = Logger.getLogger(EntityNotFoundMapper.class);

	@Override
	public Response toResponse(EntityNotFoundException e) {
		log.error("Impossible de récuperer l'entité avec l'id passé en param", e);
		return Response.status(Status.NOT_FOUND)
				.header("Server Message", "Impossible de trouver l'element recherche").build();
	}

}
