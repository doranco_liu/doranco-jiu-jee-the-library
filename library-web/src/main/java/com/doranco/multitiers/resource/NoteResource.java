package com.doranco.multitiers.resource;

import java.net.URI;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.INote;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.mappers.NoteMapper;
import com.doranco.multitiers.model.vm.BookVM;
import com.doranco.multitiers.model.vm.NoteVM;

@Path("notes")
@Produces(MediaType.APPLICATION_JSON)
public class NoteResource {
	Logger log = Logger.getLogger(NoteResource.class);

	NoteMapper noteMapper = NoteMapper.INSTANCE;

	INote iNote = null;

	public NoteResource() throws LibraryException {
		super();

		try {
			log.debug("Avant de NoteResource lookup");
			iNote = (INote) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/NoteBean");
			log.debug("Fin de NoteResource constructeur!");
		} catch (NamingException e) {
			log.error("Impossible de démarrer de la resource NoteResource!", e);
			throw new LibraryException(e);
		}
	}

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(NoteVM noteVM) throws LibraryException {
		Note note = null;
		note = noteMapper.vMToEntity(noteVM);
		log.debug("noteVM --> note: " + note);
		note = iNote.create(note);
		log.debug("note après de la creation: " + note);
		noteVM = noteMapper.entityToVM(note);
		log.debug("note --> noteVM: " + noteVM);
		
		
//		noteVM = noteMapper.entityToVM(iNote.create(noteMapper.vMToEntity(noteVM)));

		URI uri = UriBuilder.fromResource(NoteResource.class).build();

		return Response.created(uri).entity(noteVM)
				.header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Note creee avec succes").build();
	}

	@PUT
	public Response update(NoteVM noteVM) throws LibraryException {

		noteVM = noteMapper.entityToVM(iNote.update(noteMapper.vMToEntity(noteVM)));

		URI uri = UriBuilder.fromResource(NoteResource.class).build();

		return Response.created(uri).entity(noteVM)
				.header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Note modifiee avec succes").build();
	}

	@DELETE
	public Response delete(@QueryParam("userId") long userId, @QueryParam("bookId") long bookId)
			throws LibraryException {
		
		NoteVM noteVM = noteMapper.entityToVM(iNote.findByUserIdAndBookId(userId, bookId));
		
		iNote.delete(userId, bookId);
		
		URI uri = UriBuilder.fromResource(NoteResource.class).build();

		return Response.created(uri).entity(noteVM)
				.header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Suppression effectuee avec succes").build();

//		return Response.ok().header(LibraryWebConstants.RESPONSE_HEADER_MESSAGE, "Suppression effectuee avec succes")
//				.build();
	}
	
	
	@Path("findByUserIdAndBookId")
	@GET
	public Response findByUserIdAndBookId(
			@QueryParam("userId") long userId, 
			@QueryParam("bookId") long bookId) 
			throws LibraryException {
		
		NoteVM noteVM = null;
		Note note = null;
		
		log.debug("Trying to get a note form its userId ==" + userId + ", bookId==" + bookId);
		note = iNote.findByUserIdAndBookId(userId, bookId);
		log.debug("Le note troué est: " + note);
		noteVM = noteMapper.entityToVM(note);
		
		return Response.ok().entity(noteVM).build();
	}

}
