package com.doranco.multitiers.resource;

import java.net.URI;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IOrder;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.mappers.OrderMapper;
import com.doranco.multitiers.model.vm.OrderVM;

@Path("orders")
@Produces(MediaType.APPLICATION_JSON)
public class OrderResource {

	Logger log = Logger.getLogger(OrderResource.class);

	OrderMapper orderMapper = OrderMapper.INSTANCE;
	
	IOrder iOrder = null;
	
	public OrderResource() throws LibraryException {
		super();
		
		try {
			log.debug("Avant de OrderResource lookup");
			iOrder = (IOrder) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/OrderBean");
			log.debug("Fin de OrderResource constructeur!");
		} catch (NamingException e) {
			log.error("Impossible de démarrer de la resource OrderResource!", e);
			throw new LibraryException(e);
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createOrder(OrderVM orderVM) throws LibraryException {
		
		Order order = null;
		log.debug("Try to create un order a partir d'un orderVM: " + orderVM);

		order = orderMapper.vMToEntity(orderVM);
		log.debug("orderVM-->order=" + order);
		
		order = iOrder.create(order);
		log.debug("order apres la creation est: " + order);
		
		orderVM = orderMapper.entityToVM(order);
		log.debug("order-->orderVM==" + orderVM);

		URI uri = UriBuilder.fromResource(OrderResource.class).build();
		 
		return Response.created(uri).entity(orderVM).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "order cré avec succès").build();
		
//		return Response.ok().entity(orderVM).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "order cré avec succès").build();
		
	}
	
	
}
