package com.doranco.multitiers.resource;

import java.net.URI;
import java.util.List;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IBook;
import com.doranco.multitiers.ejb.interfaces.INote;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.ejb.interfaces.Panier;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.doranco.multitiers.mappers.BookMapper;
import com.doranco.multitiers.mappers.NoteMapper;
import com.doranco.multitiers.mappers.UserMapper;
import com.doranco.multitiers.model.vm.BookVM;
import com.doranco.multitiers.model.vm.CredentialsVM;
import com.doranco.multitiers.model.vm.NoteVM;
import com.doranco.multitiers.model.vm.UserVM;
import com.doranco.multitiers.resource.security.AuthenticationChecked;
import com.doranco.multitiers.resource.security.SuperAdminAuthChecked;
import com.doranco.multitiers.utils.Page;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

	Logger log = Logger.getLogger(UserResource.class);

	@Context
	UriInfo uriInfo;

	UserMapper userMapper = UserMapper.INSTANCE;
	BookMapper bookMapper = BookMapper.INSTANCE;
	NoteMapper noteMapper = NoteMapper.INSTANCE;

	IUser iUser = null;
	IBook iBook = null;
	INote iNote = null;
	Panier panier;

	public UserResource() throws LibraryException {
		super();

		try {
			log.debug("Avant de UserResource lookup");
			iUser = (IUser) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/UserBean");
			iBook = (IBook) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/BookBean");
			iNote = (INote) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/NoteBean");
			log.debug("Fin de UserResource constructeur!");
		} catch (NamingException e) {
			log.error("Impossible de démarrer de la resource UserResource!", e);
			throw new LibraryException(e);
		}
	}

	@Path("suscribe")
	@POST
	public User suscribe(User user) {

		try {
			user = iUser.suscribe(user);
		} catch (LibraryException e) {
			log.error("Impossible la resource UserResource", e);
			// TODO send a custom message to the user;
		}

		return user;
	}

	@Path("createNote")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public NoteVM createNote(NoteVM noteVM) {

		Note note = null;
		try {
			note = noteMapper.vMToEntity(noteVM);

			note = iNote.create(note);

			noteVM = noteMapper.entityToVM(note);
		} catch (LibraryException e) {
			log.error("Impossible la resource UserResource", e);
			// TODO send a custom message to the user;
		}

		return noteVM;
	}

	// @POST
	// @Consumes(MediaType.APPLICATION_JSON)
	// public BookVM createBook(BookVM bookVM) {
	//
	// Book book = null;
	// try {
	// book = bookMapper.vMToEntity(bookVM);
	// book = iBook.createBook(book);
	//
	// bookVM = bookMapper.entityToVM(book);
	// } catch (LibraryException e) {
	// log.error("Impossible la resource UserResource", e);
	// // TODO send a custom message to the user;
	// }
	//
	// return bookVM;
	// }

	// @POST
	// @Consumes(MediaType.APPLICATION_JSON)
	// public UserVM createUser(UserVM userVM) {
	//
	// User user = null;
	// try {
	// user = userMapper.vMToEntity(userVM);
	// user = iUser.createUser(user);
	//
	// userVM = userMapper.entityToVM(user);
	// } catch (LibraryException e) {
	// log.error("Impossible la resource UserResource", e);
	// // TODO send a custom message to the user;
	// }
	//
	// return userVM;
	// }

	// @GET
	// @Consumes(MediaType.APPLICATION_JSON)
	// public UserVM createUser(@QueryParam("userName") String userName,
	// @QueryParam("password") String password,
	// @QueryParam("firstName") String firstName, @QueryParam("lastName") String
	// lastName) {
	//
	// UserVM userVM = new UserVM();
	// userVM.setFirstName(firstName);
	// userVM.setLastName(lastName);
	// userVM.setUserName(userName);
	// userVM.setPassword(password);
	//
	// User user = null;
	// try {
	// user = userMapper.vMToEntity(userVM);
	// user.setPassword("password");
	// user = iUser.createUser(user);
	//
	// userVM = userMapper.entityToVM(user);
	// } catch (LibraryException e) {
	// log.error("Impossible la resource UserResource", e);
	// // TODO send a custom message to the user;
	// }
	//
	// return userVM;
	// }


	// @Path("{id}")
	// @GET
	// @SecurityChecked //attachée par le nom à l’annotation
	// public UserVM getUser(@PathParam("id") long id) {
	// UserVM userVM = null;
	// User user = null;
	//
	// log.debug("Trying to get a user form its id =="+id);
	//
	// try {
	//
	// user = iUser.getUser(id);
	// log.debug("Le user troué dans la methode getUser est: " + user);
	// userVM = userMapper.entityToVM(user);
	// } catch (LibraryException e) {
	// log.error("Impossible de recup un user", e);
	// }
	//
	// return userVM;
	//
	// }

	// @GET
	// public List<User> getAll() {
	// List<User> users = null;
	//
	// try {
	// users = (List<User>) iUser.getAll();
	// log.debug("Dans la methode getAll, users sont: " + users);
	// } catch (LibraryException e) {
	// log.error("Impossible de récupérer tous les users", e);
	// }
	//
	// return users;
	// }

	// @GET
	// public List<UserVM> getAllVM(@QueryParam("pageSize") int pageSize,
	// @QueryParam("pageNumber") int pageNumber) {
	//
	// List<User> users = null;
	// List<UserVM> userVMs = null;
	//
	//
	// try {
	// users = (List<User>) iUser.getAll();
	// userVMs = UserMapper.INSTANCE.entitiesToVMs(users);
	//
	// log.debug("Dans la methode getAll, users sont: " + users);
	// } catch (LibraryException e) {
	// log.error("Impossible de récupérer tous les users", e);
	// }
	//
	// return userVMs;
	// }

	// @GET
	// public Page<UserVM> getAllVM(@QueryParam("pageSize") int pageSize,
	// @QueryParam("pageNumber") int pageNumber) {
	//
	// Page<User> page = null;
	// Page<UserVM> pageVM = null;
	//
	// try {
	// log.debug("dans la methode getAllVM, pageSize=" + pageSize +", pageNumber=" +
	// pageNumber);
	// page = iUser.find(pageSize, pageNumber);
	// pageVM = userMapper.entitiesPageToVMsPage(page);
	//
	// log.debug("Dans la methode getAllVM, page sont: " + page.getContent());
	// } catch (LibraryException e) {
	// log.error("Impossible de récupérer tous les users", e);
	// }
	//
	// return pageVM;
	// }

	@Path("login")
	@POST
	public Response authenticate(CredentialsVM cred) throws LibraryException {
		String token = null;
		token = iUser.connect(cred.getUserName(), cred.getPassword(), uriInfo.getAbsolutePath().toString());
		return Response.ok().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).build();

	}
	
	@Path("{id}")
	@GET
	@AuthenticationChecked // attachée par le nom à l’annotation
	public Response getUser(@PathParam("id") long id) throws LibraryException {
		UserVM userVM = null;
		User user = null;

		log.debug("Trying to get a user form its id ==" + id);

		user = iUser.getUser(id);
		log.debug("Le user troué dans la methode getUser est: " + user);
		userVM = userMapper.entityToVM(user);

		return Response.ok().entity(user).build();

	}
	
	@Path("getAll")
	@GET
	public Response getAll() throws LibraryException {
		List<User> users = null;
		List<UserVM> userVMs = null;
		
		log.debug("Trying to get all users dans la BD");
		users = (List<User>) iUser.getAll();
		log.debug("Les users trouvés sont: " + users);
		userVMs = userMapper.entitiesToVMs(users);
		
		GenericEntity<List<UserVM>> entity = new GenericEntity<List<UserVM>>(userVMs) {};
		
		return Response.ok().entity(entity).build();
	}

	@Path("createUser")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createUser(UserVM userVM) throws LibraryException {
		User user = null;
		log.debug("Try to create un utilisateur a partir d'un userVM: " + userVM);

		user = userMapper.vMToEntity(userVM);
		
		user = iUser.createUser(user);
		
		userVM = userMapper.entityToVM(user);

		return Response.ok().entity(userVM).header("server message", "utilisateur cré avec succès").build();
		
//		URI uri = UriBuilder.fromResource(UserResource.class).build();
//		 
//		return Response.created(uri).entity(userVM).header("server message", "utilisateur cré avec succès").build();
	}

	@Path("getAllVM")
	@GET
	public Response getAllVM(@QueryParam("pageSize") int pageSize, @QueryParam("pageNumber") int pageNumber)
			throws LibraryException {

		Page<User> page = null;
		Page<UserVM> pageVM = null;

		log.debug("dans la methode getAllVM, pageSize=" + pageSize + ", pageNumber=" + pageNumber);
		page = iUser.find(pageSize, pageNumber);
		pageVM = userMapper.entitiesPageToVMsPage(page);

		log.debug("Dans la methode getAllVM, page sont: " + page.getContent());

		return Response.ok().entity(page).build();
	}
	
	@Path("setAsAdmin/{id}")
	@GET
	@SuperAdminAuthChecked
	public Response setAsAdmin(@PathParam("id") long id) throws LibraryException{
		
		iUser.setAsAdmin(id);
		
		return Response.ok()
				.header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "L'utilisateur est désormais un adminstrateur")
				.build();
	}

}
