package com.doranco.multitiers.resource.exception.mappers;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

@Provider
public class NotAuthorizedMapper implements ExceptionMapper<NotAuthorizedException>{

	Logger log = Logger.getLogger(NotAuthorizedMapper.class);
	
	@Override
	public Response toResponse(NotAuthorizedException e) {
		log.error("Acces a la resource refuse", e);
		return Response.status(Status.UNAUTHORIZED)
				.header("Server Message", "Acces a la resource refuse").build();
	}

}
