package com.doranco.multitiers.resource;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IBook;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.mappers.BookMapper;
import com.doranco.multitiers.model.vm.BookVM;
import com.doranco.multitiers.resource.security.AdminAuthenticationChecked;
import com.doranco.multitiers.utils.Page;

@Path("books")
@Produces(MediaType.APPLICATION_JSON)
public class BookResource {

	Logger log = Logger.getLogger(BookResource.class);

	BookMapper bookMapper = BookMapper.INSTANCE;
	
	IBook iBook = null;
	
	public BookResource() throws LibraryException {
		super();
		
		try {
			log.debug("Avant de BookResource lookup");
			iBook = (IBook) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/BookBean");
			log.debug("Fin de BookResource constructeur!");
		} catch (NamingException e) {
			log.error("Impossible de démarrer de la resource BookResource!", e);
			throw new LibraryException(e);
		}
	}
	
	
	@Path("createBook")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@AdminAuthenticationChecked
	public Response createBook(BookVM bookVM) throws LibraryException {
		Book book = null;
		log.debug("Try to create un book a partir d'un bookVM: " + bookVM);

		book = bookMapper.vMToEntity(bookVM);
		
		book = iBook.createBook(book);
		
		bookVM = bookMapper.entityToVM(book);

		return Response.ok().entity(bookVM).header("server message", "book cré avec succès").build();
	}
	
	
	@POST
	@Consumes({MediaType.MULTIPART_FORM_DATA})
//	@AdminAuthenticationChecked
	public Response createBook(@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetails, 
			@FormDataParam("bookMetadata") FormDataBodyPart jsonPart) 
			throws LibraryException, IOException {
		
		jsonPart.setMediaType(MediaType.APPLICATION_JSON_TYPE);
		BookVM bookVM = jsonPart.getValueAs(BookVM.class);
		
		log.info("file details >>>>> " + fileDetails.toString());
		byte[] fileContent=IOUtils.toByteArray(fileInputStream);
		
		log.info("file content lenght >>>>>>> " + fileContent.length);
		
		bookVM.setContent(fileContent);
		
		bookVM = bookMapper.entityToVM(iBook.createBook(bookMapper.vMToEntity(bookVM)));
		
		URI uri = UriBuilder.fromResource(BookResource.class).build();
		
		return Response.created(uri).entity(bookVM)
				.header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "book cré avec succès").build();
	}
	
	
	@Path("updateBook")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@AdminAuthenticationChecked
	public Response updateBook(BookVM bookVM) 
			throws LibraryException {
		Book book = null;
		log.debug("Try to modifier un livre a partir d'un bookVM: " + bookVM);

		book = bookMapper.vMToEntityUpdate(bookVM);
		
		book = iBook.updateBook(book);
		
		bookVM = bookMapper.entityToVM(book);

		return Response.ok().entity(bookVM).header("server message", "book modifié avec succès").build();
	}
	
	@Path("deleteBook")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@AdminAuthenticationChecked
	public Response deleteBook(BookVM bookVM) 
			throws LibraryException {
		Book book = null;
		log.debug("Try to supprimer un livre a partir d'un bookVM: " + bookVM);

		book = bookMapper.vMToEntityUpdate(bookVM);
		
		book = iBook.deleteBook(book);
		
		bookVM = bookMapper.entityToVM(book);

		return Response.ok().entity(bookVM).header("server message", "book modifié avec succès").build();
	}
	
	@Path("{id}")
	@GET
	public Response getBook(@PathParam("id") long id) throws LibraryException {
		BookVM bookVM = null;
		Book book = null;

		log.debug("Trying to get a book form its id ==" + id);

		book = iBook.findBook(id);
		log.debug("Le book troué est: " + book);
		bookVM = bookMapper.entityToVM(book);

		return Response.ok().entity(bookVM).build();
	}
	
	@Path("getBooks/{queryString}")
	@GET
	public Response getBooks(@PathParam("queryString") String queryString, @PathParam("pageSize") int pageSize, @PathParam("pageNumber") int pageNumber) throws LibraryException {
		Page<BookVM> bookVMs = null;
		Page<Book> books = null;

		log.debug("Trying to get the books according to queryString ==" + queryString);

		books = iBook.findBooks(queryString, pageSize, pageNumber);
		log.debug("Les books troués sont: " + books);
		bookVMs = bookMapper.entitiesPageToVMsPage(books);
		
		GenericEntity<Page<BookVM>> entity = new GenericEntity<Page<BookVM>>(bookVMs) {};

		return Response.ok().entity(entity).build();
	}
	
	@Path("getAll")
	@GET
	public Response getAll() throws LibraryException {
		List<Book> books = null;
		List<BookVM> bookVMs = null;
		
		log.debug("Trying to get all books dans la BD");
		books = (List<Book>) iBook.findAll();
		log.debug("Les books trouvés sont: " + books);
		bookVMs = bookMapper.entitiesToVMs(books);
		
		GenericEntity<List<BookVM>> entity = new GenericEntity<List<BookVM>>(bookVMs) {};
		
		return Response.ok().entity(entity).build();
	}
	
	@Path("viewings")
	@GET
	public Response getViewingBook(@QueryParam("idUser") long idUser, 
			@QueryParam("idBook") long idBook) 
		throws LibraryException{
		Book book = iBook.findBook(idBook); 
		BookVM bookVM = bookMapper.entityToVM(book);
		return Response.status(Status.FOUND).entity(bookVM).build();
	}
	
}
