package com.doranco.multitiers.resource.security;

import java.io.IOException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.HttpHeaders;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.utils.KeyGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class SuperAdminAuthCheckedInterceptor extends AuthenticationCheckedInterceptor {

	Logger log = Logger.getLogger(SuperAdminAuthCheckedInterceptor.class);

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		super.filter(requestContext);

		// recuperation des headers de la requête
		String autorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		log.debug("autorizationHeader:" + autorizationHeader);

		if (autorizationHeader == null || !autorizationHeader.startsWith("Bearer ")) {
			log.error("Authorization header invalide");
			throw new NotAuthorizedException("L'entête authorization devrait être fournie");
		}

		String token = autorizationHeader.substring("Bearer ".length()).trim();
		log.debug("token:" + token);

		Key key;
		try {
			key = KeyGenerator.getInstance().getKey();
			log.debug("key dans la methode filter():" + key);
			Claims jwtBody = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();

			List<String> roles = (List<String>) jwtBody.get(LibraryConstants.JWT_ROLE_KEY);

			if (!roles.contains(LibraryConstants.SUPER_ADMIN_ROLE)) {
				log.error("Role invalide");
				throw new NotAuthorizedException("La liste de role ne contient pas le role super administrateur");
			}

		} catch (NoSuchAlgorithmException e) {
			log.error("Erreur lors de la validation du token", e);
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		log.info("token valide : " + token);
	}

}
