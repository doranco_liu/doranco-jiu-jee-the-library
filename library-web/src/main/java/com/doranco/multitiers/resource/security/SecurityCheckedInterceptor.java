package com.doranco.multitiers.resource.security;

import java.io.IOException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;

import org.apache.log4j.Logger;

import com.doranco.multitiers.utils.KeyGenerator;

import io.jsonwebtoken.Jwts;

//Filtre qui sera attaché ssi la ressource est marquée (annotée)
public class SecurityCheckedInterceptor implements ContainerRequestFilter {

	Logger log = Logger.getLogger(SecurityCheckedInterceptor.class);
	

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		//recuperation des headers de la requête
		String autorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		log.debug("autorizationHeader:" + autorizationHeader);
		
		if(autorizationHeader==null || !autorizationHeader.startsWith("Bearer ")) {
			log.error("Authorization header invalide");
			throw new NotAuthorizedException("L'entête authorization devrait être fournie");
		}
		
		String token = autorizationHeader.substring("Bearer ".length()).trim();
		log.debug("token:" + token);
		
		Key key;
		try {
			key = KeyGenerator.getInstance().getKey();
			log.debug("key dans la methode filter():"+ key);
			Jwts.parser().setSigningKey(key).parseClaimsJws(token);
		} catch (NoSuchAlgorithmException e) {
			log.error("Erreur lors de la validation du token", e);
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		log.info("token valide : " + token);
		
	}
	
	
	
}
