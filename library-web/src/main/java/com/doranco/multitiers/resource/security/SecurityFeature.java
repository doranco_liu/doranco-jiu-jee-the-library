package com.doranco.multitiers.resource.security;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

@Provider
public class SecurityFeature implements DynamicFeature {

	@Override
	public void configure(ResourceInfo resourceInfo, FeatureContext context) {
		if (resourceInfo.getResourceMethod().isAnnotationPresent(AuthenticationChecked.class)) {
			// enregistrement du filtre réél
			context.register(AuthenticationCheckedInterceptor.class);
		}

		if (resourceInfo.getResourceMethod().isAnnotationPresent(AdminAuthenticationChecked.class)) {
			// enregistrement du filtre réél
			context.register(AdminAuthenticationCheckedInterceptor.class);
		}

		if (resourceInfo.getResourceMethod().isAnnotationPresent(SuperAdminAuthChecked.class)) {
			// enregistrement du filtre réél
			context.register(SuperAdminAuthCheckedInterceptor.class);
		}
	}
}
