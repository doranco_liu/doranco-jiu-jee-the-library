package com.doranco.multitiers.model.vm;

import java.io.Serializable;
import java.util.Date;

public class NoteVM implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6001966946692389042L;
	private long idBook;
	private long idUser;
	
	private Date noteDate;
	private String comment;
	private int value;
	
	public NoteVM() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public long getIdBook() {
		return idBook;
	}
	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}
	public long getIdUser() {
		return idUser;
	}
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}
	public Date getNoteDate() {
		return noteDate;
	}
	public void setNoteDate(Date noteDate) {
		this.noteDate = noteDate;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	
}
