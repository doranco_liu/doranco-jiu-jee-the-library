package com.doranco.multitiers.model.vm;

import java.io.Serializable;

public class CredentialsVM implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1610118325581006608L;
	private String userName;
	private String password;
	public CredentialsVM() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
