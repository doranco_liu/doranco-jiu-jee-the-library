package com.doranco.multitiers.model.vm;

import java.io.Serializable;

public class OrderLineVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7335023505923778029L;
	private long idBook;
//	private long idOrder;

	public OrderLineVM() {
		super();
	}

	public OrderLineVM(long idBook) {
		super();
		this.idBook = idBook;
	}

	public long getIdBook() {
		return idBook;
	}

	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

//	public long getIdOrder() {
//		return idOrder;
//	}
//
//	public void setIdOrder(long idOrder) {
//		this.idOrder = idOrder;
//	}
//
//	@Override
//	public String toString() {
//		return "OrderLineVM [idBook=" + idBook + ", idOrder=" + idOrder + "]";
//	}
	
	

}
