package com.doranco.multitiers.model.vm;

import java.io.Serializable;
import java.util.List;

public class BookVM implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2417252166167702466L;

	private long id;

	private String isbn;
	private String title;
	private byte[] content;
		
	public BookVM() {
		super();
		// TODO Auto-generated constructor stub
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	
	
}
