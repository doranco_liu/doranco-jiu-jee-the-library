package com.doranco.multitiers.model.vm;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

public class UserVM implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7932600103573359421L;

	private long id;
	
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	
	
	
	public UserVM() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	private boolean isAAdmin;
	
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public boolean isAAdmin() {
		return isAAdmin;
	}
	public void setAAdmin(boolean isAAdmin) {
		this.isAAdmin = isAAdmin;
	}
	
	
}
