package com.doranco.multitiers.model.vm;

import java.io.Serializable;
import java.util.Date;

public class ViewingVM implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2875724318545390666L;

	private long idBook;
	private long idUser;
	
	private Date startDate;
	private long duration;
	public ViewingVM() {
		super();
		// TODO Auto-generated constructor stub
	}
	public long getIdBook() {
		return idBook;
	}
	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public long getIdUser() {
		return idUser;
	}
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}
	
	
}
