package com.doranco.multitiers;

public class LibraryWebConstants {

	public static final String RESPONSE_HEADER_MESSAGE = "Server Message";
	
	public static final String RESPONSE_HEADER_AUTHORIZATION = "Authorization";
}
