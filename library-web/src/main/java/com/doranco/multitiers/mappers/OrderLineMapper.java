package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.model.vm.OrderLineVM;

@Mapper
public interface OrderLineMapper {

	OrderLineMapper INSTANCE = Mappers.getMapper( OrderLineMapper.class );
	
	@Mapping(source = "idBook", target = "book.id")
	OrderLine vmToEntity(OrderLineVM vm); 
	
//	@Mappings({
//		@Mapping(source = "book.id", target = "idBook"),
//		@Mapping(source = "order.id", target = "idOrder")})
	@Mapping(source = "book.id", target = "idBook")
	OrderLineVM  entityToVM(OrderLine entity);
	
	List<OrderLineVM> entitiesToVMs(List<OrderLine> entities);
}
