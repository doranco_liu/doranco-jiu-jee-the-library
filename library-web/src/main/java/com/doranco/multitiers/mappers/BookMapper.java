package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.model.vm.BookVM;
import com.doranco.multitiers.utils.Page;

@Mapper(uses= {NoteMapper.class, ViewingMapper.class})
//@Mapper
public interface BookMapper {

	BookMapper INSTANCE = Mappers.getMapper( BookMapper.class );

	BookVM  entityToVM(Book entity);
	
	@Mapping(target = "id", ignore=true)
	Book  vMToEntity(BookVM bookVM);
	
	//Cette methode n'utilise que pour update()
	Book  vMToEntityUpdate(BookVM bookVM);
	
	List<BookVM> entitiesToVMs(List<Book> entities);
	
	Page<BookVM> entitiesPageToVMsPage(Page<Book> page);
	
}
