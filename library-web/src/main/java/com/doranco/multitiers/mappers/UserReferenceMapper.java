package com.doranco.multitiers.mappers;

import javax.enterprise.context.ApplicationScoped;
import javax.naming.Reference;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.mapstruct.TargetType;

import com.doranco.multitiers.entity.Identifier;
import com.doranco.multitiers.entity.User;

@ApplicationScoped // CDI component model
public class UserReferenceMapper {

	@PersistenceContext
	private EntityManager entityManager;

	public User resolve(Reference reference, @TargetType User user) {
		return reference != null ? entityManager.find(user.getClass(), Long.parseLong(reference.toString())) : null;
	}
	
//	public <T extends Identifier> T resolve(Reference reference, @TargetType Class<T> entityClass) {
//        return reference != null ? entityManager.find( entityClass, Long.parseLong(reference.toString())) : null;
//    }

	public Reference toReference(Identifier identifier) {
		return identifier != null ? new Reference(Long.toString(identifier.getId())) : null;
	}
}
