package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.model.vm.UserVM;
import com.doranco.multitiers.utils.Page;

@Mapper(uses= {NoteMapper.class, OrderMapper.class, ViewingMapper.class})
//@Mapper
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );
	
	@Mapping(target = "password", ignore=true)
	UserVM  entityToVM(User entity);
	
	@Mapping(target = "id", ignore=true)
	User vMToEntity(UserVM userVM);
	
	List<UserVM> entitiesToVMs(List<User> entities);
	
	Page<UserVM> entitiesPageToVMsPage(Page<User> page);
}
