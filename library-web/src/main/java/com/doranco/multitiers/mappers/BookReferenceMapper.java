package com.doranco.multitiers.mappers;

import javax.enterprise.context.ApplicationScoped;
import javax.naming.Reference;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.mapstruct.TargetType;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Identifier;

@ApplicationScoped
public class BookReferenceMapper {

	@PersistenceContext
	private EntityManager entityManager;

	public Book resolve(Reference reference, @TargetType Book book) {
		return reference != null ? entityManager.find(book.getClass(), Long.parseLong(reference.toString())) : null;
	}

	public Reference toReference(Identifier identifier) {
		return identifier != null ? new Reference(Long.toString(identifier.getId())) : null;
	}
}
