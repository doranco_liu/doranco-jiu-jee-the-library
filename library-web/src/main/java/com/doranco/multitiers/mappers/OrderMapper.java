package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.model.vm.OrderVM;

@Mapper(uses=OrderLineMapper.class)
public interface OrderMapper {
	
	OrderMapper INSTANCE = Mappers.getMapper( OrderMapper.class );

//	@Mappings({
//		@Mapping(source = "user.id", target = "idUser"),
//		@Mapping(source = "id", target = "id")})
	@Mapping(source = "user.id", target = "idUser")
	OrderVM  entityToVM(Order entity);
	
	@Mappings({
		@Mapping(source = "idUser", target = "user.id"),
//		@Mapping(source = "orderLineVMs", target = "orderLines"),
		@Mapping(target = "id", ignore=true)})
//	@Mapping(source = "idUser", target = "user.id")
	Order  vMToEntity(OrderVM orderVM);
	
//	@Mapping(source = "book.id", target = "idBook")
//	OrderLineVM  entityToVM(OrderLine entity);
	
	List<OrderVM> entitiesToVMs(List<Order> entities);
	
	List<Order> vMsToEntities(List<OrderVM> vMs);
}
